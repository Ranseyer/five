# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Dimitar Petrovski <dimeptr@gmail.com>, 2009
#
msgid ""
msgstr ""
"Project-Id-Version: VDR-1.7.14\n"
"Report-Msgid-Bugs-To: <>\n"
"POT-Creation-Date: 2010-10-19 22:16+0200\n"
"PO-Revision-Date: 2010-10-16 08:40+0100\n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team: Macedonian <en@li.org>\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

msgid "Volume"
msgstr "Глас"

msgid "Mute"
msgstr "Без звук"

#~ msgid "Setup"
#~ msgstr "Уредување"

#~ msgid "Timers"
#~ msgstr "Тајмери"

#~ msgid "Commands"
#~ msgstr "Наредби"

#~ msgid "Recordings"
#~ msgstr "Снимки"

#~ msgid "Schedule"
#~ msgstr "Распоред"

#~ msgid "Channels"
#~ msgstr "Канали"
