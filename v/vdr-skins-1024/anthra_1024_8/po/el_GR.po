# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Dimitrios Dimitrakos <mail@dimitrios.de>, 2002, 2006
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.6.0\n"
"Report-Msgid-Bugs-To: <>\n"
"POT-Creation-Date: 2011-01-21 18:07+0100\n"
"PO-Revision-Date: 2010-11-11 22:09+0100\n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team: Greek\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-7\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Volume"
msgstr "������ "

msgid "Mute"
msgstr "�������"

#~ msgid "Channels"
#~ msgstr "�������"

#~ msgid "Timers"
#~ msgstr "��������������������"

#~ msgid "Schedule"
#~ msgstr "���������"

#~ msgid "Setup"
#~ msgstr "���������"

#~ msgid "Commands"
#~ msgstr "�������"

#~ msgid "Recordings"
#~ msgstr "��������"

#~ msgid "Channel"
#~ msgstr "������"

#~ msgid "*** Invalid Channel ***"
#~ msgstr "*** ����� ������ ***"

#, fuzzy
#~ msgid "Channel locked by LNB!"
#~ msgstr "�� ������ ����� ������������ (������ �������)!"

#~ msgid "Channel not available!"
#~ msgstr "�� ������ ��� ����� ���������!"

#~ msgid "Can't start Transfer Mode!"
#~ msgstr "�������� ��������� ���������� ���������!"

#~ msgid "off"
#~ msgstr "�������"

#~ msgid "auto"
#~ msgstr "��������"

#~ msgid "none"
#~ msgstr "������"

#~ msgid "Polarization"
#~ msgstr "������"

#~ msgid "Srate"
#~ msgstr "Srate"

#~ msgid "Inversion"
#~ msgstr "����������"

#~ msgid "CoderateH"
#~ msgstr "������ ������ H"

#~ msgid "CoderateL"
#~ msgstr "������ ������ L"

#~ msgid "Modulation"
#~ msgstr "����������"

#~ msgid "Bandwidth"
#~ msgstr "����� ����������"

#~ msgid "Transmission"
#~ msgstr "��������"

#~ msgid "Guard"
#~ msgstr "���������"

#~ msgid "Hierarchy"
#~ msgstr "���������"

#~ msgid "Starting EPG scan"
#~ msgstr "���� ������ EPG"

#~ msgid "No title"
#~ msgstr "����� �����"

#~ msgid "LanguageName$English"
#~ msgstr "��������"

#~ msgid "LanguageCode$eng"
#~ msgstr "ell"

#~ msgid "Phase 1: Detecting RC code type"
#~ msgstr "���� 1: ��������� ������ RC"

#~ msgid "Press any key on the RC unit"
#~ msgstr "����� ��� ������� ��� ���������������"

#~ msgid "RC code detected!"
#~ msgstr "������� ������� RC!"

#~ msgid "Do not press any key..."
#~ msgstr "��� ����� �������..."

#~ msgid "Phase 2: Learning specific key codes"
#~ msgstr "���� 2: �������� ����������� ��������"

#~ msgid "Press key for '%s'"
#~ msgstr "����� �� ������� ��� '%s'"

#~ msgid "Press 'Up' to confirm"
#~ msgstr "����� '����' ��� �������"

#~ msgid "Press 'Down' to continue"
#~ msgstr "����� '����' ��� ��������"

#~ msgid "(press 'Up' to go back)"
#~ msgstr "(����� '����' ��� ���������"

#~ msgid "(press 'Down' to end key definition)"
#~ msgstr "(����� '����' ��� ���������� �������� ��������)"

#~ msgid "(press 'Menu' to skip this key)"
#~ msgstr "����� '�����' ��� ���������� ����� ��� ��������"

#~ msgid "Learning Remote Control Keys"
#~ msgstr "�������� �������� �������������"

#~ msgid "Phase 3: Saving key codes"
#~ msgstr "���� 3: ���������� �������"

#~ msgid "Press 'Up' to save, 'Down' to cancel"
#~ msgstr "����� '����' ��� ����������, '����' ��� �������"

#~ msgid "Key$Up"
#~ msgstr "����"

#~ msgid "Key$Down"
#~ msgstr "����"

#~ msgid "Key$Menu"
#~ msgstr "�����"

#~ msgid "Key$Ok"
#~ msgstr "��"

#~ msgid "Key$Back"
#~ msgstr "����"

#~ msgid "Key$Left"
#~ msgstr "��������"

#~ msgid "Key$Right"
#~ msgstr "�����"

#~ msgid "Key$Red"
#~ msgstr "�������"

#~ msgid "Key$Green"
#~ msgstr "�������"

#~ msgid "Key$Yellow"
#~ msgstr "�������"

#~ msgid "Key$Blue"
#~ msgstr "����"

#~ msgid "Key$Info"
#~ msgstr "�����������"

#~ msgid "Key$Play"
#~ msgstr "�����������"

#~ msgid "Key$Pause"
#~ msgstr "�����"

#~ msgid "Key$Stop"
#~ msgstr "T����"

#~ msgid "Key$Record"
#~ msgstr "E������"

#~ msgid "Key$FastFwd"
#~ msgstr "�������� ������"

#~ msgid "Key$FastRew"
#~ msgstr "�������� ����"

#~ msgid "Key$Power"
#~ msgstr "K�������"

#~ msgid "Key$Channel+"
#~ msgstr "������+"

#~ msgid "Key$Channel-"
#~ msgstr "������-"

#~ msgid "Key$Volume+"
#~ msgstr "������+"

#~ msgid "Key$Volume-"
#~ msgstr "������-"

#~ msgid "Key$Audio"
#~ msgstr "����"

#~ msgid "Key$Schedule"
#~ msgstr "���������"

#~ msgid "Key$Channels"
#~ msgstr "�������"

#~ msgid "Key$Timers"
#~ msgstr "��������������������"

#~ msgid "Key$Recordings"
#~ msgstr "��������"

#~ msgid "Key$Setup"
#~ msgstr "���������"

#~ msgid "Key$Commands"
#~ msgstr "�������"

#~ msgid "Disk"
#~ msgstr "������"

#~ msgid "free"
#~ msgstr "���������"

#~ msgid "Edit channel"
#~ msgstr "����������� ��������"

#~ msgid "Name"
#~ msgstr "�����"

#~ msgid "Source"
#~ msgstr "����"

#~ msgid "Frequency"
#~ msgstr "���������"

#~ msgid "Vpid"
#~ msgstr "B����� PID"

#~ msgid "Ppid"
#~ msgstr "Ppid"

#~ msgid "Apid1"
#~ msgstr "Apid1"

#~ msgid "Apid2"
#~ msgstr "Apid2"

#~ msgid "Dpid1"
#~ msgstr "Dpid1"

#~ msgid "Dpid2"
#~ msgstr "Dpid2"

#~ msgid "Tpid"
#~ msgstr "Tpid"

#~ msgid "CA"
#~ msgstr "CA"

#~ msgid "Sid"
#~ msgstr "Sid"

#, fuzzy
#~ msgid "Rid"
#~ msgstr "Sid"

#~ msgid "Channel settings are not unique!"
#~ msgstr "�� ��������� ��� �������� ����������������!"

#~ msgid "Button$Edit"
#~ msgstr "����������"

#~ msgid "Button$New"
#~ msgstr "N��"

#~ msgid "Button$Delete"
#~ msgstr "��������"

#~ msgid "Button$Mark"
#~ msgstr "�������"

#~ msgid "Channel is being used by a timer!"
#~ msgstr "�� ������ ��������������� ��� �������������������!"

#~ msgid "Delete channel?"
#~ msgstr "�������� ��������?"

#~ msgid "Edit timer"
#~ msgstr "����������� ��������������������"

#~ msgid "Active"
#~ msgstr "������"

#~ msgid "Day"
#~ msgstr "�����"

#~ msgid "Start"
#~ msgstr "����"

#~ msgid "Stop"
#~ msgstr "�����"

#~ msgid "VPS"
#~ msgstr "VPS"

#~ msgid "Priority"
#~ msgstr "�������������"

#~ msgid "Lifetime"
#~ msgstr "�������� ���������"

#~ msgid "yes"
#~ msgstr "���"

#~ msgid "no"
#~ msgstr "���"

#~ msgid "File"
#~ msgstr "������"

#~ msgid "First day"
#~ msgstr "����� ����"

#~ msgid "Button$On/Off"
#~ msgstr "A����/K������"

#~ msgid "Button$Info"
#~ msgstr "�����������"

#~ msgid "Delete timer?"
#~ msgstr "�������� ��������������������;?"

#~ msgid "Timer still recording - really delete?"
#~ msgstr "�������������������� �� ������� - �������� �������?"

#~ msgid "Event"
#~ msgstr "�������"

#~ msgid "Button$Record"
#~ msgstr "�������"

#~ msgid "Button$Switch"
#~ msgstr "A�����"

#~ msgid "What's on now?"
#~ msgstr "������ ���������"

#~ msgid "What's on next?"
#~ msgstr "������� ���������"

#~ msgid "Button$Next"
#~ msgstr "�������"

#~ msgid "Button$Now"
#~ msgstr "����"

#~ msgid "Button$Schedule"
#~ msgstr "���������"

#~ msgid "Can't switch channel!"
#~ msgstr "������ �������� �������!"

#~ msgid "Schedule - %s"
#~ msgstr "��������� - %s"

#~ msgid "Recording info"
#~ msgstr "����������� E�������"

#~ msgid "Button$Play"
#~ msgstr "�����������"

#~ msgid "Button$Rewind"
#~ msgstr "�������� ���� ����"

#, fuzzy
#~ msgid "Rename recording"
#~ msgstr "�������� ��������?"

#, fuzzy
#~ msgid "Date"
#~ msgstr "Srate"

#, fuzzy
#~ msgid "PES"
#~ msgstr "VPS"

#, fuzzy
#~ msgid "Delete marks information?"
#~ msgstr "�������� ��������?"

#, fuzzy
#~ msgid "Delete resume information?"
#~ msgstr "�������� ��������?"

#~ msgid "Error while accessing recording!"
#~ msgstr "�������� ���� ���������� ��������!"

#~ msgid "Button$Open"
#~ msgstr "�������"

#~ msgid "Delete recording?"
#~ msgstr "�������� ��������?"

#~ msgid "Error while deleting recording!"
#~ msgstr "����� ���� ��� �������� ��� �������!"

#~ msgid "Recording commands"
#~ msgstr "������� ��� ��������"

#~ msgid "never"
#~ msgstr "����"

#~ msgid "skin dependent"
#~ msgstr "�������� ��� ��� ���������"

#~ msgid "always"
#~ msgstr "�����"

#~ msgid "OSD"
#~ msgstr "OSD"

#~ msgid "Setup.OSD$Language"
#~ msgstr "������"

#~ msgid "Setup.OSD$Skin"
#~ msgstr "���������"

#~ msgid "Setup.OSD$Theme"
#~ msgstr "����"

#, fuzzy
#~ msgid "Setup.OSD$WarEagle icons"
#~ msgstr "������"

#~ msgid "Setup.OSD$Left (%)"
#~ msgstr "�������� (%)"

#~ msgid "Setup.OSD$Top (%)"
#~ msgstr "����� (%)"

#~ msgid "Setup.OSD$Width (%)"
#~ msgstr "������ (%)"

#~ msgid "Setup.OSD$Height (%)"
#~ msgstr "���� (%)"

#~ msgid "Setup.OSD$Message time (s)"
#~ msgstr "������ �������� ��������� (�)"

#~ msgid "Setup.OSD$Use small font"
#~ msgstr "������������� ������ ��������������"

#~ msgid "Setup.OSD$Channel info position"
#~ msgstr "���� ����������� ��������"

#~ msgid "bottom"
#~ msgstr "����"

#~ msgid "top"
#~ msgstr "����"

#~ msgid "Setup.OSD$Channel info time (s)"
#~ msgstr "������ ��������� ����������� �������� �� (�)"

#~ msgid "Setup.OSD$Info on channel switch"
#~ msgstr "����������� ���� ������ ��������"

#~ msgid "Setup.OSD$Scroll pages"
#~ msgstr "������ �������"

#~ msgid "Setup.OSD$Scroll wraps"
#~ msgstr "������ ����-����"

#~ msgid "Setup.OSD$Recording directories"
#~ msgstr "������� ��������"

#, fuzzy
#~ msgid "Setup.OSD$Main menu command position"
#~ msgstr "���� ����������� ��������"

#, fuzzy
#~ msgid "Setup.OSD$Show valid input"
#~ msgstr "���������"

#~ msgid "EPG"
#~ msgstr "������������ ������ ������������"

#~ msgid "Button$Scan"
#~ msgstr "������"

#~ msgid "Setup.EPG$EPG scan timeout (h)"
#~ msgstr "������ ��������� �������� EPG �� ����"

#~ msgid "Setup.EPG$EPG bugfix level"
#~ msgstr "������ ��������� ������ EPG"

#~ msgid "Setup.EPG$EPG linger time (min)"
#~ msgstr "������� ������������ ����������� (�����)"

#~ msgid "Setup.EPG$Set system time"
#~ msgstr "����������� ���� ����������"

#~ msgid "Setup.EPG$Use time from transponder"
#~ msgstr "������������ ����������� ����"

#~ msgid "Setup.EPG$Preferred languages"
#~ msgstr "������������� �������"

#~ msgid "Setup.EPG$Preferred language"
#~ msgstr "������������ ������"

#~ msgid "pan&scan"
#~ msgstr "pan&scan"

#~ msgid "letterbox"
#~ msgstr "letterbox"

#~ msgid "center cut out"
#~ msgstr "center cut out"

#~ msgid "names only"
#~ msgstr "���� �������"

#~ msgid "names and PIDs"
#~ msgstr "������� ��� PIDs"

#~ msgid "add new channels"
#~ msgstr "�������� ���� ��������"

#~ msgid "add new transponders"
#~ msgstr "�������� ���� �����������"

#~ msgid "DVB"
#~ msgstr "DVB"

#, fuzzy
#~ msgid "Setup.DVB$Use DVB receivers"
#~ msgstr "����� ���� Dolby Digital"

#~ msgid "Setup.DVB$Primary DVB interface"
#~ msgstr "����� DVB �����"

#~ msgid "Setup.DVB$Video format"
#~ msgstr "����� ������"

#~ msgid "Setup.DVB$Video display format"
#~ msgstr "����� ����������� ������"

#~ msgid "Setup.DVB$Use Dolby Digital"
#~ msgstr "����� ���� Dolby Digital"

#~ msgid "Setup.DVB$Update channels"
#~ msgstr "��������� ��������"

#, fuzzy
#~ msgid "Setup.DVB$channel binding by Rid"
#~ msgstr "���� ����������� ��������"

#~ msgid "Setup.DVB$Audio languages"
#~ msgstr "������� ����"

#~ msgid "Setup.DVB$Audio language"
#~ msgstr "������ ����"

#~ msgid "LNB"
#~ msgstr "LNB"

#, fuzzy
#~ msgid "Setup.LNB$Log LNB usage"
#~ msgstr "���� LNB-��������� (MHz)"

#~ msgid "Setup.LNB$Use DiSEqC"
#~ msgstr "������������ DiSEqC"

#~ msgid "Setup.LNB$SLOF (MHz)"
#~ msgstr "SLOF (MHz)"

#~ msgid "Setup.LNB$Low LNB frequency (MHz)"
#~ msgstr "���� LNB-��������� (MHz)"

#~ msgid "Setup.LNB$High LNB frequency (MHz)"
#~ msgstr "��� LNB-��������� (MHz)"

#~ msgid "CAM"
#~ msgstr "CAM"

#~ msgid "Button$Menu"
#~ msgstr "M����"

#~ msgid "Button$Reset"
#~ msgstr "���������"

#~ msgid "Can't open CAM menu!"
#~ msgstr "������� � �������� ��� CAM �����!"

#~ msgid "Can't reset CAM!"
#~ msgstr "������� � ��������� ��� CAM"

#~ msgid "Recording"
#~ msgstr "�������"

#~ msgid "Setup.Recording$Margin at start (min)"
#~ msgstr "��������� ������ ���� ���� (�����)"

#~ msgid "Setup.Recording$Margin at stop (min)"
#~ msgstr "��������� ������ ��� ����� (�����)"

#~ msgid "Setup.Recording$Primary limit"
#~ msgstr "�������� ����"

#~ msgid "Setup.Recording$Default priority"
#~ msgstr "�������������� �������������"

#~ msgid "Setup.Recording$Default lifetime (d)"
#~ msgstr "�������������� �������� ��������� (������)"

#~ msgid "Setup.Recording$Pause priority"
#~ msgstr "������������� ������������"

#~ msgid "Setup.Recording$Pause lifetime (d)"
#~ msgstr "�������� �����������"

#, fuzzy
#~ msgid "Setup.Recording$Video directory policy"
#~ msgstr "������� ��������"

#, fuzzy
#~ msgid "Setup.Recording$Number of video directories"
#~ msgstr "������� ��������"

#, fuzzy
#~ msgid "Setup.Recording$Video %d priority"
#~ msgstr "������������� ������������"

#, fuzzy
#~ msgid "Setup.Recording$Video %d min. free MB"
#~ msgstr "������� ������� ������� (MB)"

#~ msgid "Setup.Recording$Use episode name"
#~ msgstr "����� �������� ����������"

#~ msgid "Setup.Recording$Use VPS"
#~ msgstr "����� VPS"

#~ msgid "Setup.Recording$VPS margin (s)"
#~ msgstr "��������� VPS (�)"

#~ msgid "Setup.Recording$Mark instant recording"
#~ msgstr "������� ��������� ��������"

#~ msgid "Setup.Recording$Name instant recording"
#~ msgstr "�������� ��������� ��������"

#~ msgid "Setup.Recording$Instant rec. time (min)"
#~ msgstr "�������� ��������� �������� (�����)"

#~ msgid "Setup.Recording$Max. video file size (MB)"
#~ msgstr "������� ������� ������� (MB)"

#, fuzzy
#~ msgid "Setup.Recording$Max. recording size (GB)"
#~ msgstr "������� ������� ������� (MB)"

#, fuzzy
#~ msgid "Setup.Recording$Hard Link Cutter"
#~ msgstr "������� ��������� ��������"

#~ msgid "Setup.Recording$Split edited files"
#~ msgstr "����������� �������������� �������"

#, fuzzy
#~ msgid "Setup.Recording$Show date"
#~ msgstr "����������� �������������� �������"

#, fuzzy
#~ msgid "Setup.Recording$Show time"
#~ msgstr "�������� �����������"

#, fuzzy
#~ msgid "Setup.Recording$Show length"
#~ msgstr "����� VPS"

#~ msgid "Replay"
#~ msgstr "�����������"

#~ msgid "Setup.Replay$Multi speed mode"
#~ msgstr "������� ��������� ���������"

#~ msgid "Setup.Replay$Show replay mode"
#~ msgstr "������� ���������� ������������"

#~ msgid "Setup.Replay$Resume ID"
#~ msgstr "ID ������������"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds"
#~ msgstr "����� VPS"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds Slow"
#~ msgstr "����� VPS"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds (Repeat)"
#~ msgstr "����� �������� ����������"

#, fuzzy
#~ msgid "Setup.Replay$Jump&Play"
#~ msgstr "ID ������������"

#, fuzzy
#~ msgid "Setup.Replay$Play&Jump"
#~ msgstr "ID ������������"

#, fuzzy
#~ msgid "Setup.Replay$Pause at last mark"
#~ msgstr "������� ��������� ���������"

#, fuzzy
#~ msgid "Setup.Replay$Reload marks"
#~ msgstr "ID ������������"

#, fuzzy
#~ msgid "Setup.Miscellaneous$only in channelinfo"
#~ msgstr "��������� ������ ���������� (�����)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$only in progress display"
#~ msgstr "������� ������� (�)"

#~ msgid "Miscellaneous"
#~ msgstr "�������"

#~ msgid "Setup.Miscellaneous$Min. event timeout (min)"
#~ msgstr "��������� ������ ���������� (�����)"

#~ msgid "Setup.Miscellaneous$Min. user inactivity (min)"
#~ msgstr "��������� ������ �������� (�����)"

#~ msgid "Setup.Miscellaneous$SVDRP timeout (s)"
#~ msgstr "SVDRP ������� (�)"

#~ msgid "Setup.Miscellaneous$Zap timeout (s)"
#~ msgstr "������� ������� (�)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat delay"
#~ msgstr "��������� ������ ���������� (�����)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat freq"
#~ msgstr "������� ������� (�)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat timeout"
#~ msgstr "��������� ������ ���������� (�����)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Volume ctrl with left/right"
#~ msgstr "��������� ������ �������� (�����)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Channelgroups with left/right"
#~ msgstr "������� ������� (�)"

#~ msgid "Plugins"
#~ msgstr "����������"

#~ msgid "This plugin has no setup parameters!"
#~ msgstr "���� � �������� ��� ���� �����������!"

#~ msgid "Restart"
#~ msgstr "������������"

#~ msgid "Really restart?"
#~ msgstr "N� ����� ������� ������������?"

#~ msgid " Stop recording "
#~ msgstr " ����� ������� "

#~ msgid "VDR"
#~ msgstr "VDR"

#~ msgid " Stop replaying"
#~ msgstr " ����� ������������"

#~ msgid "Button$Audio"
#~ msgstr "����"

#~ msgid "Button$Pause"
#~ msgstr "�����"

#~ msgid "Button$Stop"
#~ msgstr "�����"

#~ msgid "Button$Resume"
#~ msgstr "���������"

#~ msgid " Cancel editing"
#~ msgstr " ������� ������������"

#~ msgid "Stop recording?"
#~ msgstr "������� ��������?"

#~ msgid "Cancel editing?"
#~ msgstr "A������ ������������?"

#~ msgid "No audio available!"
#~ msgstr "�� ���������� ����"

#~ msgid "No free DVB device to record!"
#~ msgstr "���������� DVB ������ ��� �������!"

#~ msgid "Pausing live video..."
#~ msgstr "������ �������� �������"

#~ msgid "Jump: "
#~ msgstr "����������: "

#~ msgid "No editing marks defined!"
#~ msgstr "��� ����� ������� ������ ������������"

#~ msgid "Can't start editing process!"
#~ msgstr "�������� ��������� ��� ������������!"

#~ msgid "Editing process started"
#~ msgstr "������ � �����������"

#~ msgid "Editing process already active!"
#~ msgstr "����������� ��������� �� �������!"

#~ msgid "FileNameChars$ abcdefghijklmnopqrstuvwxyz0123456789-.,#~\\^$[]|()*+?{}/:%@&"
#~ msgstr " ��������������������������������0123456789-.,#~\\^$[]|()*+?{}/:%@&abcdefghijklmnopqrstuvwxyz"

#~ msgid "Button$ABC/abc"
#~ msgstr "AB�/���"

#~ msgid "Button$Overwrite"
#~ msgstr "�������������"

#~ msgid "Button$Insert"
#~ msgstr "��������"

#~ msgid "Plugin"
#~ msgstr "��������"

#~ msgid "Up/Dn for new location - OK to move"
#~ msgstr "����/���� ��� ��� ����. ���� ��"

#~ msgid "Channel locked (recording)!"
#~ msgstr "�� ������ ����� ������������ (������ �������)!"

#~ msgid "Low disk space!"
#~ msgstr "� ������� �������� �� �������!"

#~ msgid "Can't shutdown - option '-s' not given!"
#~ msgstr "�������� �� ����� �����������. ��������� � ���������� '-s'!"

#~ msgid "Recording - shut down anyway?"
#~ msgstr "������� ������� - ������ �� ����� �����������?"

#~ msgid "Recording in %ld minutes, shut down anyway?"
#~ msgstr "���������� ������� �� %ld ����� - ������ �� �����������?"

#~ msgid "shut down anyway?"
#~ msgstr "������ �� ����� �����������?"

#~ msgid "Recording - restart anyway?"
#~ msgstr "������� ������� - ������ �� ����� ������������?"

#~ msgid "restart anyway?"
#~ msgstr "������ �� ����� ������������?"

#~ msgid "Classic VDR"
#~ msgstr "������� VDR"

#~ msgid "ST:TNG Panels"
#~ msgstr "������ ST:TNG"

#~ msgid "MTWTFSS"
#~ msgstr "�������"

#~ msgid "MonTueWedThuFriSatSun"
#~ msgstr "������������������K��"

#~ msgid "Monday"
#~ msgstr "�������"

#~ msgid "Tuesday"
#~ msgstr "�����"

#~ msgid "Wednesday"
#~ msgstr "�������"

#~ msgid "Thursday"
#~ msgstr "������"

#~ msgid "Friday"
#~ msgstr "���������"

#~ msgid "Saturday"
#~ msgstr "�������"

#~ msgid "Sunday"
#~ msgstr "�������"

#~ msgid "Press any key to cancel shutdown"
#~ msgstr "����� ��� ������� ��� ������� �����������"

#~ msgid "Switching primary DVB..."
#~ msgstr "� ����� DVB ����� �������..."

#~ msgid "Editing process failed!"
#~ msgstr "� ����������� �������!"

#~ msgid "Editing process finished"
#~ msgstr "� ����������� ��������"
